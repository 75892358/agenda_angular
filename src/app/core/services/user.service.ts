import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map } from 'rxjs';
import { User } from '../model/enteties/User.model';
import { SoUser } from '../model/so/So-user.model';


@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private http: HttpClient) {}
    User(formData:FormData){

      return this.http.post('https://localhost:7036/User/Insert',formData);
    }
    getAll(){
      return this.http.get('https://localhost:7036/User/getall').
      pipe(map((Response) =>Response as SoUser))


    }

}
