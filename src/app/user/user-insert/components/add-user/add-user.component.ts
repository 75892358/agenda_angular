import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { FormBuilder } from '@angular/forms';
import { Validators } from '@angular/forms';
import { UserService } from 'src/app/core/services/user.service';
@Component({
  selector: 'app-add-user',
  templateUrl: './add-user.component.html',
  styleUrls: ['./add-user.component.css'],
})
export class AddUserComponent implements OnInit {
  form: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    private userService: UserService,
  ) {
    this.form = this.formBuilder.group({
    //  iduser: ['', [Validators.required]],
      password: ['', [Validators.required]],
      Nombres: ['', Validators.required],
      Apellidos: ['', null],
      Dni: ['', Validators.required],
      NumeroTelefonico: ['', Validators.required],
      Direccion: ['', Validators.required],
      EmailUsuario: ['', [Validators.required,Validators.email]],
      FechaNacimiento: ['',Validators.required],
      CodigoPostal: ['', Validators.required],
    });
  }
  ngOnInit(): void {}
  buildForm() {

  }
  user() {
    if (this.form.valid) {
      const formData = new FormData();
     // formData.append('dto.idUsuario',this.form.controls['iduser'].value);
      formData.append('dtoUser.idContraseña',this.form.controls['password'].value);
      formData.append('dtoUser.Nombres',this.form.controls['Nombres'].value);
      formData.append('dtoUser.Apellidos', this.form.controls['Apellidos'].value);
      formData.append('dtoUser.Dni', this.form.controls['Dni'].value);
      formData.append('dtoUser.NumeroTelefonico', this.form.controls['NumeroTelefonico'].value);
      formData.append('dtoUser.Direccion', this.form.controls['Direccion'].value);
      formData.append('dtoUser.EmailUsuario', this.form.controls['EmailUsuario'].value);
      formData.append('dtoUser.FechaNacimiento', this.form.controls['FechaNacimiento'].value);
      formData.append('dtoUser.CodigoPostal', this.form.controls['CodigoPostal'].value);
      this.userService.User(formData).subscribe((rpt) => {
        console.log(rpt);
      });
    }
}
}
